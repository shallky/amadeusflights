﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AmadeusNet.Startup))]
namespace AmadeusNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
