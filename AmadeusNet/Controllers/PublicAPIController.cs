﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace AmadeusNet.Controllers
{
    public class PublicAPIController : Controller
    {
        [HttpGet]
        public string GetPersons()
        {
            string query = "select * from baza";

            var osobe = new List<Osoba>();
            osobe.Add(new Osoba { Ime = "Ime1", Prezime ="Prezime1"});
            osobe.Add(new Osoba { Ime = "Ime2", Prezime = "Prezime2" });
            osobe.Add(new Osoba { Ime = "Ime3", Prezime = "Prezime3" });
            osobe.Add(new Osoba { Ime = "Ime4", Prezime = "Prezime4" });

            return JsonConvert.SerializeObject(osobe);
        }

        class Osoba
        {
            public string Ime { get; set; }
            public string Prezime { get; set; }
        }
    }
}
