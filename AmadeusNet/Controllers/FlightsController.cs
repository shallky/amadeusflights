﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using AmadeusNet.Models.Flights;
using log4net;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace AmadeusNet.Controllers
{
    
    public class FlightsController : Controller
    {
        // GET: Flights
        public ActionResult Index()
        {
            var model = new FlightSearchModel();
            return View(model);
        }


        /// <summary>
        /// Post action for searching Amadeus Sandbox for Low-fare flights
        /// </summary>
        /// <param name="filter">Search parameters</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult Search(FlightSearchModel filter)
        {
            try
            {
                RootResult model = null;
                //try to find object in cache
                if (ModelState.IsValid)
                {
                   
                    var cached = (RootResult) WebCache.Get(filter.GetCacheKey);
                    if (cached != null)
                    {
                        return PartialView("FlightResultsPartial", cached);
                    }

                    
                    using (
                        var client =
                            new HttpClient(new HttpClientHandler
                            {
                                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                            }))
                    {

                        NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

                        queryString["apikey"] = Config.ApiKey;
                        queryString["origin"] = filter.Origin;
                        queryString["destination"] = filter.Destination;
                        queryString["adults"] = filter.NumberAdults.ToString();
                        queryString["children"] = filter.NumberChildren.ToString();
                        queryString["departure_date"] = filter.DepartureDate.ToString("yyyy-MM-dd");
                        if (filter.IsReturn)
                            queryString["return_date"] = filter.ReturnDate.ToString("yyyy-MM-dd");
                        queryString["number_of_results"] = filter.NumberOfResults.ToString();
                        queryString["currency"] = filter.Currency;
                        queryString["infants"] = filter.NumberInfants.ToString();

                        client.BaseAddress = new Uri(Config.ApiUrl);
                        HttpResponseMessage response = client.GetAsync("?" + queryString).Result;
                        response.EnsureSuccessStatusCode();
                        string result = response.Content.ReadAsStringAsync().Result;

                        //deserializing Response
                        model = JsonConvert.DeserializeObject<RootResult>(result);

                        //cache object, defult expiration policy
                        if (model != null)
                        {
                            WebCache.Set(filter.GetCacheKey, model);
                        }
                    }
                }

                return PartialView("FlightResultsPartial", model);
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                LogManager.GetLogger(typeof(FlightsController)).Fatal(ex.Message);
                return PartialView("FlightResultsPartial", null);
            }
           
        }
    }
}
