﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmadeusNet.Models.Flights
{
    public class FlightSearchModel
    {
        public FlightSearchModel()
        {
            Origin = "BOS";
            Destination = "LON";

            DepartureDate = DateTime.Now.AddDays(1);
            ReturnDate = DateTime.Now.AddDays(3);

            NumberAdults = 1;
            NumberChildren = 0;
            NumberInfants = 0;

            IsReturn = false;
        }

        [Required(ErrorMessage = "Origin is required")]
        public string Origin { get; set; }
        [Required(ErrorMessage = "Destination is required")]
        public string Destination { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DepartureDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReturnDate { get; set; }

        [Required(ErrorMessage = "Number of adults is required")]
        [Range(1, int.MaxValue)]
        public int NumberAdults { get; set; }

        [Required(ErrorMessage = "Number of children is required")]
        [Range(0, int.MaxValue)]
        public int NumberChildren { get; set; }

        [Required(ErrorMessage = "Number of infants is required")]
        [Range(0, int.MaxValue)]
        public int NumberInfants { get; set; }

        public int NumberOfResults => 10;

        public string Currency => "EUR";

        public bool IsReturn { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        protected bool Equals(FlightSearchModel other)
        {
            return string.Equals(Origin, other.Origin) && string.Equals(Destination, other.Destination) && DepartureDate.Equals(other.DepartureDate) && ReturnDate.Equals(other.ReturnDate) && NumberAdults == other.NumberAdults && NumberChildren == other.NumberChildren && NumberInfants == other.NumberInfants && IsReturn == other.IsReturn;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Origin != null ? Origin.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Destination != null ? Destination.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ DepartureDate.GetHashCode();
                hashCode = (hashCode*397) ^ ReturnDate.GetHashCode();
                hashCode = (hashCode*397) ^ NumberAdults;
                hashCode = (hashCode*397) ^ NumberChildren;
                hashCode = (hashCode*397) ^ NumberInfants;
                hashCode = (hashCode*397) ^ IsReturn.GetHashCode();
                return hashCode;
            }
        }


        /// <summary>
        /// Creates cache key based on class name and hashcode
        /// </summary>
        public string GetCacheKey => $"{this.GetType().Name}_{this.GetHashCode()}";
    }
}